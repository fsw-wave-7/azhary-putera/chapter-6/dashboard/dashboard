const { join } = require('path')
const bcrypt = require('bcrypt')
const { User } = require('../../models')


class AuthController {
    
    login = (req, res) =>{
        res.render(join(__dirname, '../../views/login'))
    }
    
    doLogin = async (req, res) => {
        const body = req.body;

        if (!(body.username && body.password)) {
            return res.status(400).send({ error: "Data not formatted properly" });
        }

        User.findOne({
            where: {username: body.username }
        })
        .then(user => {
            bcrypt.compare(body.password, user.password, (err, data) => {
                if (err) throw err

                if (data) {
                    res.cookie('loginData', JSON.stringify(user))
                    res.redirect('/')
                } else {
                    return res.status(401).json({ msg: "Invalid credencial" })
                }

            });
        })
        .catch(err => {
            return res.status(401).json({ msg: "Invalid credencial" })
        })
    }

    
    logout =  (req, res) =>{
        res.clearCookies('loginData')
        res.redirect('/')
    }
    
}

module.exports = AuthController