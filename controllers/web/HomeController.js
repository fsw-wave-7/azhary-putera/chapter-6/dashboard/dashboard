const { join } = require('path')
const { User } = require('../../models')
const bcrypt = require('bcrypt')

class HomeController {

    index = (req, res) =>{
        User.findAll()
        .then(user => {   
        res.render(join(__dirname, '../../views/index'),{
        content: './pages/userlist',
        user : user
            })
        })
    }

    add = (req, res)=>{
        res.render(join(__dirname, '../../views/index'),{
        content: './pages/addlist'
    })
    }
    
    saveUser = async (req, res)=> {
        const salt =await bcrypt.genSalt(10)

        User.create({
            name: req.body.name,
            username: req.body.username,
            age: req.body.age,
            password: await bcrypt.hash(req.body.password, salt)
        })
        .then(() => {
            res.redirect('/')
        }) .catch(err => {
            console.log(err)
        })
    }

    editUser = (req, res) => {
        
        const index = req.params.id;

            User.findOne({
                where: { id: index },
              })
              .then((user) => {
                res.render(join(__dirname, '../../views/index'),{
                content: './pages/editUser',
                user : user,
            })

        }) .catch(err => {
            console.log(err)
        })  
    }

    updateUser = (req, res) => {
    User.update({
            name: req.body.name,
            username: req.body.username,
            age: req.body.age,
            password: req.body.password
        }, {
        where: { id: req.params.id }
        })
        .then(user => {
            res.redirect('/')
        }) .catch(err => {
            res.status(422).json("Can't update user")
        })

    }

    // deleteUser =
    deleteUser = (req, res) => {
        User.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(() => {
            res.redirect('/')
        }) 

    }
    
}

module.exports = HomeController