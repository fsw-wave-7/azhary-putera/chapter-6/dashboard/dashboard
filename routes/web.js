const { Router } = require('express')
const bodyParser = require('body-parser')
const cookieparser = require('cookie-parser')
const Authmiddleware = require('../middlewares/Authmiddleware')


const AuthController = require('../controllers/web/Authcontroller')
const HomeController = require('../controllers/web/HomeController')


const authController = new AuthController
const homeController = new HomeController

const web = Router()
web.use(bodyParser.json())
web.use(bodyParser.urlencoded({extended: true}))

web.use(Authmiddleware)

web.get('/login', authController.login)
web.post('/login', authController.doLogin)
web.get('/logout', authController.logout)

web.get('/add', homeController.add)
web.post('/save-user', homeController.saveUser)
web.get('/', homeController.index)
web.get('/editUser/:id', homeController.editUser)
web.post('/editUser/:id', homeController.updateUser)
web.get('/deleteUser/:id', homeController.deleteUser)



module.exports = web
