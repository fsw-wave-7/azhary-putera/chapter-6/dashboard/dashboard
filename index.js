const express = require ('express')
const app = express()
const port = 3000
const path = require('path')
const { join } = require('path')
const api = require('./routes/api.js')
const web = require('./routes/web.js')


app.set('view engine', 'ejs')
app.use(express.static(__dirname + '/public'));

app.use('/api', api)
app.use('/', web)





app.listen(port, () =>{console.log('server berhasil dijalankan')})

